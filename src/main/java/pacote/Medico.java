package pacote;

public class Medico {

	private final String nome;
	private final int idade;

	public Medico(String nome, int idade) {
		this.nome = nome;
		this.idade = idade;
	}

	public String getNome() {
		return nome;
	}

	public int getIdade() {
		return idade;
	}

}
