package pacote;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MedicoViewController {
	
	@RequestMapping("/diego")
	public String getMedico(Model model) {
		model.addAttribute("medico", new Medico("Diego Lanfranchi", 31));
		return "diego";
	}

}
